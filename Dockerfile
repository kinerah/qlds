FROM cm2network/steamcmd
RUN cd /home/steam/steamcmd
RUN ./steamcmd.sh +login anonymous +force_install_dir /home/steam/qlds/ +app_update 349090 +quit
COPY entrypoint.sh /usr/local/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

